package decorator;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Data data;
		Scanner scanner = new Scanner(System.in);

		Boolean käynnissä = true;

		while (käynnissä) {
			System.out.println("Valitse pääsynvalvonnalla(1) tai ilman(2)");
			int valinta = Integer.parseInt(scanner.nextLine());
			try {
				switch (valinta) {
				case 1: {
					data = new Decorator(new DataPankki());
					System.out.println(data.getSaldo());
					break;
				}
				case 2: {
					data = new DataPankki();
					System.out.println(data.getSaldo());
					break;
				}
				default:
					throw new IllegalArgumentException("Unexpected value: " + valinta);
				}

			} catch (Exception e) {
				System.out.println(e);
			}
			käynnissä = false;
		}
		scanner.close();
	}
}
