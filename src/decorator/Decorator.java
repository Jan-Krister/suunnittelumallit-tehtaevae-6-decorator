package decorator;

import java.util.Scanner;

public class Decorator implements Data {
	
	Scanner sc = new Scanner(System.in);
	protected Data dataPankki;

	public Decorator(Data dataPankki) {
		this.dataPankki = dataPankki;
	}

	@Override
	public String getSaldo() {
		System.out.println("valitse oletko oikeutettu näkemään tilin saldon y/n");
		String pääsy = sc.nextLine();
		if (pääsy.equalsIgnoreCase("y")) {
		return dataPankki.getSaldo();
		} else {
			return "Ei pääsyä";
			
		}
	}

}
